# Game Engine

[![npm](https://img.shields.io/npm/v/@yepreally/game-engine)](https://www.npmjs.com/package/@yepreally/game-engine)
[![npm](https://img.shields.io/npm/dw/@yepreally/game-engine)](https://www.npmjs.com/package/@yepreally/game-engine)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/yepreally/game-engine?branch=master)](https://gitlab.com/yepreally/game-engine/-/pipelines)
[![Gitlab code coverage](https://img.shields.io/gitlab/coverage/yepreally/game-engine/master)](https://gitlab.com/yepreally/game-engine/-/graphs/master/charts)
[![Snyk Vulnerabilities for npm package](https://img.shields.io/snyk/vulnerabilities/npm/@yepreally/game-engine)](https://snyk.io/advisor/npm-package/@yepreally/game-engine)

## Getting Started

### Installation
```npm install @yepreally/game-engine```

### Usage
```// TODO Document```