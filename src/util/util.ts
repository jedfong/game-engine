export function chooseRandom<T>(...items: T[]): T {
  return items[Math.floor(Math.random() * items.length)];
}

export function randomFromRange(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

export function degToRad(deg: number): number {
  return deg * (Math.PI / 180);
}

export function radToDeg(rad: number): number {
  return rad * (180 / Math.PI);
}

export function repeat(times: number, fn: (i: number) => void): void {
  for (let i = 0; i < times; i++) {
    fn(i);
  }
}

/* istanbul ignore next */
export const noop = (): void => undefined;
