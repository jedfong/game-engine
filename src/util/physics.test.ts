import { GameObject } from '../objects';
import { motionAdd } from './physics';

describe.each([
  [0, 10, 0, -10],
  [45, 10, Math.sqrt(10 * 10 / 2), -Math.sqrt(10 * 10 / 2)],
  [90, 10, 10, 0],
  [135, 10, Math.sqrt(10 * 10 / 2), Math.sqrt(10 * 10 / 2)],
  [180, 10, 0, 10],
  [225, 10, -Math.sqrt(10 * 10 / 2), Math.sqrt(10 * 10 / 2)],
  [270, 10, -10, 0],
  [315, 10, -Math.sqrt(10 * 10 / 2), -Math.sqrt(10 * 10 / 2)],
])('motionAdd', (direction, speed, hSpeedResult, vSpeedResult) => {
  test(`motionAdd({ hSpeed: 0, vSpeed: 0 }, ${direction}, ${speed}) should return { hSpeed: ${hSpeedResult}, vSpeed: ${vSpeedResult} }`, () => {
    const obj = { hSpeed: 0, vSpeed: 0 };
    motionAdd(obj as unknown as GameObject, direction, speed);
    expect(obj.hSpeed).toBeCloseTo(hSpeedResult, 4);
    expect(obj.vSpeed).toBeCloseTo(vSpeedResult, 4);
  });
});
