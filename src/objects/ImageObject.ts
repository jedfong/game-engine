import { Game } from '../base';
import { GameObject, IGameObjectOptions } from './GameObject';
import { Sprite } from './Sprite';

export interface IImageObjectOptions extends IGameObjectOptions {
  sprite: Sprite | null,
}

export abstract class ImageObject<G extends Game = Game> extends GameObject<G> {
  sprite: Sprite | null;

  constructor(type: string, options?: Partial<IImageObjectOptions>) {
    super(type, options);
    this.sprite = options?.sprite as Sprite || null;
  }

  get width(): number {
    return this.sprite?.width || 0;
  }

  get height(): number {
    return this.sprite?.height || 0;
  }

  render(canvas: HTMLCanvasElement): void {
    this.sprite?.render(canvas, this.x, this.y, this.rotation, this.opacity);
  }
}
