import { Game } from '../base';
import { GameObject, IGameObjectOptions } from './GameObject';

export interface ITextObjectOptions extends Omit<IGameObjectOptions, 'height' | 'width'> {
  value: string,
  fontSize: string,
  fontFamily: string,
}

export abstract class TextObject<G extends Game = Game> extends GameObject<G> {
  value: string | null;
  fontSize: string;
  fontFamily: string;

  constructor(type: string, options?: Partial<ITextObjectOptions>) {
    super(type, { ...options, height: 0, width: 0 });
    this.value = options?.value ?? null;
    this.fontSize = options?.fontSize ?? '16px';
    this.fontFamily = options?.fontFamily ?? 'sans-serif';
  }

  render(canvas: HTMLCanvasElement): void {
    const ctx = canvas.getContext('2d');
    if (this.value && ctx) {
      ctx.save();
      ctx.font = `${this.fontSize} ${this.fontFamily}`;
      ctx.fillText(this.value, this.x, this.y);
      ctx.restore();
    }
  }

  addInteractions(): void {
    // default to no interactions for text
  }
}

// type ICreateTextObject = (options: Partial<ITextObjectOptions>) => ITextObject;

// const defineTextObject = (type: string, defaultOptions: Partial<ITextObjectOptions>): ICreateTextObject => {
//   const mergedOptions = { ...defaultOptions, type };
//   return options => new TextObject(type, { ...mergedOptions, ...options } as ITextObjectOptions);
// };

// const createTextObject = (type: string, options: ITextObjectOptions): ITextObject => new TextObject(type, options);

// export {
//   TextObject,
//   createTextObject,
//   defineTextObject,
// };
