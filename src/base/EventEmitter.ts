export type IEventName = string;
export type IListener<T> = (payload: T) => void;
export type IUnsubscribe = () => void;

export interface IEventEmitter {
  on: <T>(eventName: string, listener: IListener<T>) => IUnsubscribe;
  once: <T>(eventName: string, listener: IListener<T>) => IUnsubscribe;
  off: <T>(eventName: string, listener: IListener<T>) => void;
  emit: <T>(eventName: string, payload?: T) => void;
  setMaxListeners: (maxListeners: number) => void;
}

interface IListenerOptions {
  singleUse: boolean;
}

interface IListenerState<T> {
  listener: IListener<T>;
  options: IListenerOptions;
}

export interface IEventEmitterOptions {
  maxListeners?: number;
}

export class EventEmitter implements IEventEmitter {
  listeners: { [eventName: string]: IListenerState<unknown>[] };
  maxListeners: number;

  constructor(options?: IEventEmitterOptions) {
    this.listeners = {};
    this.maxListeners = options?.maxListeners ?? 10;
  }

  setMaxListeners(maxListeners: number): void {
    this.maxListeners = maxListeners;
  }

  private addListener<T>(eventName: string, listener: IListener<T>, options?: Partial<IListenerOptions>): IUnsubscribe {
    if (!this.listeners[eventName]) {
      this.listeners[eventName] = [];
    }

    this.listeners[eventName].push({
      listener: listener as IListener<unknown>,
      options: { singleUse: false, ...options },
    });

    const listenerCount = this.listeners[eventName].length;
    if (listenerCount > this.maxListeners && this.maxListeners !== 0) {
      console.warn(`Possible EventEmitter memory leak detected. ${listenerCount} ${eventName} listeners added.`
      + ` Call \'setMaxListeners\' to increase limit.`);
    }

    return () => this.off(eventName, listener);
  }

  on<T>(eventName: string, listener: IListener<T>): IUnsubscribe {
    return this.addListener(eventName, listener, { singleUse: false });
  }

  once<T>(eventName: string, listener: IListener<T>): IUnsubscribe {
    return this.addListener(eventName, listener, { singleUse: true });
  }

  off<T>(eventName: string, listener: IListener<T>): void {
    if (this.listeners[eventName]) {
      const index = this.listeners[eventName].findIndex(listenerState => listenerState.listener === listener);
      if (index > -1) {
        this.listeners[eventName].splice(index, 1);
      }
    }
  }

  emit<T>(eventName: string, payload: T): void {
    if (this.listeners[eventName]) {
      const clonedListenerStates = this.listeners[eventName].slice();
      clonedListenerStates.forEach(({ listener, options }) => {
        try {
          listener(payload);
          if (options.singleUse) {
            this.off(eventName, listener);
          }
        } catch (e) {
          console.error('Uncaught exception thrown in EventEmitter', e);
        }
      });
    }
  }
}
