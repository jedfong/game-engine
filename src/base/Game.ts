import { Screen } from './Screen';
import { Evented } from './Evented';

export interface IGameOptions {
  fpsTarget: number;
  screen: Screen | null;
}

export class Game extends Evented {
  protected options: IGameOptions;
  protected gamePane: HTMLElement;
  protected bufferCanvas: HTMLCanvasElement;
  protected canvas: HTMLCanvasElement;
  protected lastTFrame: number;
  protected tickLength: number;
  protected requestAnimationId: number | null;
  tick: number;
  fps: number;
  protected resizeObserver: ResizeObserver | null;
  protected screen!: Screen | null;

  constructor(elOrSelector: string | HTMLElement, options?: Partial<IGameOptions>) {
    super();
    this.options = this.defaultOptions(options);
    this.gamePane = this.getEl(elOrSelector);
    this.tickLength = Math.floor(1000 / this.options.fpsTarget);
    this.lastTFrame = Date.now();
    this.requestAnimationId = null;
    this.fps = 0;
    this.tick = 0;
    this.resizeObserver = null;
    this.bufferCanvas = this.createBufferCanvas();
    this.canvas = this.createCanvas();
    this.attachResizeEvents();
    this.setScreen(this.options.screen);
  }

  protected defaultOptions(options?: Partial<IGameOptions>): IGameOptions {
    return {
      fpsTarget: 60,
      screen: null,
      ...options,
    };
  }

  protected getEl(elOrSelector: string | HTMLElement): HTMLElement {
    if (elOrSelector instanceof HTMLElement) {
      return elOrSelector;
    }

    if (typeof elOrSelector === 'string') {
      const el = document.querySelector<HTMLElement>(elOrSelector);
      if (!el) {
        throw new Error(`Could not find element using selector ${elOrSelector}`);
      }
      return el;
    }

    throw new Error('elOrSelector is required');
  }

  protected createBufferCanvas(): HTMLCanvasElement {
    return document.createElement('canvas');
  }

  protected createCanvas(): HTMLCanvasElement {
    const canvas = document.createElement('canvas');
    this.gamePane.appendChild(canvas);
    this.gamePane.classList.add('game-pane');
    return canvas;
  }

  protected attachResizeEvents(): void {
    const { height, width } = this.gamePane.getBoundingClientRect();
    this.bufferCanvas.height = this.canvas.height = height;
    this.bufferCanvas.width = this.canvas.width = width;

    this.resizeObserver = new ResizeObserver(entries => entries.forEach(entry => {
      this.bufferCanvas.height = this.canvas.height = entry.contentRect.height;
      this.bufferCanvas.width = this.canvas.width = entry.contentRect.width;
    }));
    this.resizeObserver.observe(this.gamePane);
  }

  protected main(tFrame: number): void {
    const nextTick = this.tick + this.tickLength;
    let numTicks = 0;

    if (tFrame > nextTick) {
      const timeSinceTick = tFrame - this.tick;
      numTicks = Math.floor(timeSinceTick / this.tickLength);
    }

    const secondsPassed = (tFrame - this.lastTFrame) / 1000;
    this.lastTFrame = tFrame;
    this.fps = Math.round(1 / secondsPassed);

    this.update(numTicks);
    this.render();

    if (this.requestAnimationId !== null) {
      this.requestAnimationId = window.requestAnimationFrame(this.main.bind(this));
    }
  }

  protected update(numTicks: number): void {
    for (let i = 0; i < numTicks; i++) {
      this.tick = this.tick + this.tickLength;
      this.screen?.update();
      this.updateKeyboardCheckPressed();
    }
  }

  protected render(): void {
    this.bufferCanvas.getContext('2d')?.clearRect(0, 0, this.bufferCanvas.width, this.bufferCanvas.height);
    this.screen?.render(this.bufferCanvas);
    const context2d = this.canvas.getContext('2d') as CanvasRenderingContext2D;
    context2d.clearRect(0, 0, this.canvas.width, this.canvas.height);
    context2d.drawImage(this.bufferCanvas, 0, 0);
  }

  get height(): number {
    return this.canvas.height;
  }

  get width(): number {
    return this.canvas.width;
  }

  // TODO stop setScreen from loading/unloading/etc

  setScreen(screen: Screen | null): void {
    if (this.screen !== screen) {
      this.screen?.unload(this);
      this.screen = screen as Screen || null;
      this.screen?.load(this);
    }
  }

  start(): void {
    if (this.requestAnimationId === null) {
      this.tick = performance.now();
      this.requestAnimationId = window.requestAnimationFrame(this.main.bind(this));
    }
  }

  stop(): void {
    if (this.requestAnimationId !== null) {
      window.cancelAnimationFrame(this.requestAnimationId);
      this.requestAnimationId = null;
    }
  }

  destroy(): void {
    this.stop();
    this.resizeObserver?.disconnect();
    this.resizeObserver = null;
    this.gamePane.removeChild(this.canvas);
    this.gamePane.removeChild(this.bufferCanvas);
  }
}
