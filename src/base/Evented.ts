const keysDown: { [key: string]: boolean } = {};
const keyPressHandled: { [key: string]: boolean } = {};

export function keyboardCheck(keycode: string): boolean {
  return !!keysDown[keycode];
}

export function keyboardCheckPressed(keycode: string): boolean {
  return keysDown[keycode] && !keyPressHandled[keycode];
}

export class Evented {
  constructor() {
    window.addEventListener('keydown', event => {
      keysDown[event.key] = true;
    }, true);

    window.addEventListener('keyup', event => {
      delete keysDown[event.key];
      delete keyPressHandled[event.key];
    }, true);
  }

  protected updateKeyboardCheckPressed(): void {
    Object.keys(keysDown).forEach(keycode => {
      keyPressHandled[keycode] = true;
    });
  }
}
