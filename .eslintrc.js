module.exports = {
  extends: [
    '@yepreally/eslint-config/core',
    '@yepreally/eslint-config/import',
    '@yepreally/eslint-config/jest',
    '@yepreally/eslint-config/typescript',
  ],
};
